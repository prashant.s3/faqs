-- MySQL dump 10.13  Distrib 8.0.24, for Win64 (x86_64)
--
-- Host: localhost    Database: faq
-- ------------------------------------------------------
-- Server version	8.0.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `faq_x_projects`
--

DROP TABLE IF EXISTS `faq_x_projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `faq_x_projects` (
  `id` int NOT NULL AUTO_INCREMENT,
  `faq_id` int DEFAULT NULL,
  `projects_id` int DEFAULT NULL,
  `faq_status` char(2) DEFAULT 'I',
  PRIMARY KEY (`id`),
  KEY `faq_id` (`faq_id`),
  KEY `projects_id` (`projects_id`),
  CONSTRAINT `faq_x_projects_ibfk_1` FOREIGN KEY (`faq_id`) REFERENCES `faq` (`faq_id`),
  CONSTRAINT `faq_x_projects_ibfk_2` FOREIGN KEY (`projects_id`) REFERENCES `projects` (`projects_id`),
  CONSTRAINT `CHK_faq_x_projects` CHECK (((`faq_status` = _utf8mb4'A') or (`faq_status` = _utf8mb4'I')))
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faq_x_projects`
--

LOCK TABLES `faq_x_projects` WRITE;
/*!40000 ALTER TABLE `faq_x_projects` DISABLE KEYS */;
INSERT INTO `faq_x_projects` VALUES (1,1,1,'A'),(2,2,2,'A'),(3,3,3,'A'),(4,4,4,'A'),(5,5,5,'A'),(6,6,NULL,'A'),(7,7,NULL,'A'),(8,8,NULL,'A'),(9,9,NULL,'A'),(10,10,NULL,'A'),(11,11,NULL,'A'),(12,12,NULL,'A'),(13,13,NULL,'A'),(14,14,NULL,'A'),(15,15,NULL,'A'),(16,16,NULL,'A'),(17,17,NULL,'A'),(18,18,NULL,'A'),(19,19,NULL,'A');
/*!40000 ALTER TABLE `faq_x_projects` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-03 10:31:08
