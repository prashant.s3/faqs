-- MySQL dump 10.13  Distrib 8.0.24, for Win64 (x86_64)
--
-- Host: localhost    Database: faq
-- ------------------------------------------------------
-- Server version	8.0.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `faq`
--

DROP TABLE IF EXISTS `faq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `faq` (
  `faq_id` int NOT NULL AUTO_INCREMENT,
  `question` varchar(255) DEFAULT NULL,
  `answer` varchar(1000) DEFAULT NULL,
  `createdby` varchar(30) DEFAULT NULL,
  `created_date` date DEFAULT (curdate()),
  `updated_by` varchar(30) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `faq_status` char(2) DEFAULT 'A',
  `faq_type` enum('G','S') DEFAULT 'G',
  PRIMARY KEY (`faq_id`),
  UNIQUE KEY `question_UNIQUE` (`question`),
  CONSTRAINT `CHK_faq` CHECK (((`faq_status` = _utf8mb4'A') or (`faq_status` = _utf8mb4'I')))
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faq`
--

LOCK TABLES `faq` WRITE;
/*!40000 ALTER TABLE `faq` DISABLE KEYS */;
INSERT INTO `faq` VALUES (1,'I have received my voucher. Can I give it to someone else?','Of course you can! Simply forward the voucher SMS and Email received to the person to whom you want to give the voucher to.','1','2021-06-02',NULL,NULL,'A','G'),(2,'Where can I locate the outlet, which will accept the voucher?','Vouchers bought through YES CART can be used at all listed outlets. You can find out the nearest outlet at the outlet locator section of the brand page. What’s more! You can also forward the address and phone number to your email ID and phone number.','1','2021-06-02',NULL,NULL,'A','G'),(3,'I am at the store and the cashier is not accepting the voucher.','Do not worry! Simply call us on 1800-103-3314 and we will help you out.\n\n','1','2021-06-02',NULL,NULL,'A','G'),(4,'My voucher can only be used online but it shows up as invalid. What should I do?','There are a few quick self-help checks that you should look at –\n\nAre you following the correct redemption process? Check for the correct redemption steps at the brand page on https://www.gyftr.com/hdfcsmartbuy/ as ‘How To Redeem’.\nIf the steps are correct and the error is invalid voucher code, please reach out to the helpline number of the brand. Click here to find the helpline number of your preferred brand.','1','2021-06-02',NULL,NULL,'A','G'),(5,'My voucher can only be used online but it shows as consumed. What should I do?','Please reach out to the helpline number of the voucher brand or simply call us on 1800-103-3314 and we will resolve it for you.','1','2021-06-02',NULL,NULL,'A','G'),(6,'My vouchers are expired, what should I do?','We would emphasize on always redeeming the vouchers during the validity period. Extension of validity is not possible.','1','2021-06-02',NULL,NULL,'A','S'),(7,'My voucher expired during COVID-19 lockdown. How do I use it now?','Do not worry! We have you covered. Here is what you should do –\n\nFor Book My Show vouchers – please click here and enter your details. BMS will update the validity of your vouchers within 7 days.\n\nFor Lifestyle, Shoppers Stop, Max & Westside, please visit the customer service desk of the nearest outlet and they will extend the validity of your vouchers.\n\nFor Amazon, Flipkart, Myntra, Ferns & Petals, please contact the customer service team of these brands and they will be able to assist you with extension of validity.\n\nFor any other brand apart from one’s listed above, please reach out to us at https://gyftr.com/hdfcsmartbuy/contact-us and our team will assist you with the validity extension.','1','2021-06-02',NULL,NULL,'A','S'),(8,'I want to club my multiple vouchers into one. What should I do?','This is possible in certain circumstances. Please visit https://www.gyftr.com/yescart/contact and enter your details. Simply select the brand name, choose request under Purpose and check if the ‘detailed out’ section gives an option of “I want to club my GVs”. If the brand of your choice allows clubbing of vouchers, you can raise a request there and we will quickly get it addressed for you. ','1','2021-06-02',NULL,NULL,'A','G'),(9,'How many times can a voucher be used?','Per se, most of our vouchers are one-time use. Please refer to the terms and conditions on the brand page at www.gyftr.com for detailed information.','1','2021-06-02',NULL,NULL,'A','G'),(10,'Brand or Denomination of my choice is not available. What should I do?','The catalogue is dynamic, and brand/denominations are available on the website basis the availability of inventory. While we try our best to ensure stock availability, in the event you don’t find a voucher denomination of your choice, we advise you to wait for a while and try again.','1','2021-06-02',NULL,NULL,'A','G'),(11,'Is there a physical voucher available?','We provide e-vouchers through email and SMS. The Vouchers can be used at stores or at respective brand’s website or mobile app. Since the voucher code is present in your phone, it can be redeemed conveniently. Hence, no physical vouchers are issued.','1','2021-06-02',NULL,NULL,'A','G'),(12,'How many vouchers can I use in one transaction?','This varies from brand to brand. Please visit the brand page on www.gyftr.com for exact details.','1','2021-06-02',NULL,NULL,'A','G'),(13,'I am getting the error “Transaction Failed due to Risk”. What does that mean and what should I do?','This essentially means that your transactions are following a pattern that are not in line with our internal risk assessment policy. Unfortunately, you would not be able to use this card for future purchases with us. We regret the inconvenience caused.','1','2021-06-02',NULL,NULL,'A','G'),(14,'The amount is debited from my account however, the order is not successful.','Do not worry! Your money is safe. In such instances, the orders are usually processed within 24 hours. If we have received the amount and the order does not get processed, the amount is reversed back to your account/card within 3-5 working days.\n','1','2021-06-02',NULL,NULL,'A','G'),(15,'I have not received my vouchers yet.','That can happen occasionally due to network issues. Here is what you can do – \n\nCheck if you have received the voucher SMS/email from GYFTR. This can go to your junk/spam mailbox too.\nIn case you haven’t received the vouchers, simply give us a missed call on 8510 830 830 from your registered mobile number and we will resend the vouchers to you.\nIf the vouchers are not on email, please wait for an hour after you have processed your order and follow the steps above. If you do not receive your vouchers even after an hour, call us on 1800-103-3314 and we will resolve it for you.','1','2021-06-02',NULL,NULL,'A','G'),(16,'I have lost my voucher SMS/email. What should I do?','We have you covered here! Simply give a missed call on 8510-830-830 and the last 5 vouchers issued to you will be resent to your number.','1','2021-06-02',NULL,NULL,'A','G'),(17,'I want to return/cancel a voucher I purchased.','Unfortunately, these are non-returnable vouchers. ','1','2021-06-02',NULL,NULL,'A','G'),(18,'How can I use my voucher?','Not only are our vouchers delivered instantly, but they are also very easy to redeem. A voucher that can be used at a store simply needs to be presented at the cash counter at the time of billing. The voucher amount is adjusted and the remaining amount of bill, if any, will need to be paid in cash/card. Please note that these are one time use vouchers. To make the most of it, try using vouchers if the billing value is equal to or more than the voucher amount.\n\nFor online redemptions, please visit the brand page on www.gyftr.com and check the steps of redemption.\n\nWe also have some videos of major brands on our YouTube page. Please visit bit.ly/3kPXSJs and subscribe to our channel and get to see the easy-to-understand video tutorials of various brands. We ensure that up to date videos are made available for your reference.','1','2021-06-02',NULL,NULL,'A','G'),(19,'What is the Validity of the Gift Vouchers?','The validity of each Gift Voucher varies from brand to brand and can be between 3 months to 12 months. For the exact validity, please refer to your Voucher Email or SMS.','1','2021-06-02',NULL,NULL,'A','G');
/*!40000 ALTER TABLE `faq` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-03 10:31:07
