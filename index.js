const http = require('http');
const express = require('express');
const handler = require('./handler');
const config = require('config');
const app = express();

app.use(express.json());

app.get('/faqs', handler.getall_faq);
app.post('/project_slug', handler.given_projectslug);
app.get('/faqs/:FAQ_slug', handler.get_global_or_specific_faq);
app.post('/insertfaq', handler.insertFaq);
app.post('/cat_slug', handler.category_slug);

const server = http.createServer(app);

server.listen(config.get('PORT'), () => console.log(`SERVER is ready at - http://localhost:${config.get('PORT')} 🚀`));
// app.listen(, () => console.log('App is ready at  http://127.0.0.1:3500 '));
