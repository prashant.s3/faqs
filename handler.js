const mysql = require('mysql');
const { promisify } = require('util');
const config = require('config');

exports.getall_faq = async (req, res) => {
  
    try{
  
    var connection = mysql.createConnection(config.get('db'));

    const result = await promisify(connection.query).call(connection, `call faq.getALLfaq();`, []);
  
    res.send({ slug: result[0] });
  }catch (err){
  
    console.log(err);
    res.status(404).json({
      status: 'error',
      statusCode: 404,
      error: 'No able to get data'
    });
  }finally {
    connection.end();
  }
  };

  exports.get_global_or_specific_faq = async (req, res) => {
  
    try{
  
    var connection = mysql.createConnection(config.get('db'));
  
    const { FAQ_slug } = req.params;
  
    const result = await promisify(connection.query).call(connection, `call faq.mention_the_type_of_faq('${FAQ_slug}');`, []);
  
    res.send({ slug: result[0] });
  }catch (err){
  
    console.log(err);
    res.status(404).json({
      status: 'error',
      statusCode: 404,
      error: 'No able to get data'
    });
  }finally {
    connection.end();
  }
  };

  exports.insertFaq = async(req, res) => {

    try{
       var connection = mysql.createConnection(config.get('db'));
     
       let { question, answer , createdby} = req.body;
     
     
        await promisify(connection.query).call(
         connection,
         `call faq.Insert_faq('${question}','${answer}','${createdby}');`,
         []
       );
     
       res.status(200).json({ message: "Data successfully inserted" });
        }catch (err){
   
           console.log(err);
           res.status(404).json({
             status: 'error',
             statusCode: 404,
             error: 'No able to insert data'
           });
         }finally {
           connection.end();
         }
     }
     
  exports.given_projectslug = async (req, res) => {
    try{
    var connection = mysql.createConnection(config.get('db'));
  
    const { projectSlug } = req.body;
  
    const result = await promisify(connection.query).call(connection, `call faq.get_faqs('${projectSlug}');`, []);
  
    results = result[0];
  
    res.send({ slug: results });
  }catch (err){
  
    console.log(err);
    res.status(404).json({
      status: 'error',
      statusCode: 404,
      error: 'No able to get data'
    });
  }finally {
    connection.end();
  }
  };

  exports.category_slug = async (req, res) => {
    try{
    var connection = mysql.createConnection(config.get('db'));
  
    const { given_category , given_limit } = req.body;
  
    const result = await promisify(connection.query).call(connection, `call faq.testing_procedure('${given_category}',${given_limit});`, []);
  
    results = result[0];
  
    res.send({ slug: results });
  }catch (err){
  
    console.log(err);
    res.status(404).json({
      status: 'error',
      statusCode: 404,
      error: 'No able to get data'
    });
  }finally {
    connection.end();
  }
  };