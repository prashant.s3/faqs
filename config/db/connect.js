const { promisify } = require('util');
const config = require('config');
const mysql = require('mysql');

const { host, port, name: db_name, user, pass: password } = config.get('db');
const params = {
  host,
  port,
  user,
  password,
  database: db_name,
  //   debug: true,
  multipleStatements: true,
  connectTimeout: 10000,
  acquireTimeout: 10000
  // socketPath: '/var/run/mysqld/mysqld.sock'
};

const pool = mysql.createPool({
  connectionLimit: 10,
  host,
  user,
  password,
  database: db_name
});

/*
 *** Custom Mysql query
 */

function promisifyQuery() {
  return promisify(pool.query).bind(pool);
}

async function getConnection() {
  const connection = await promisify(pool.getConnection).bind(pool)();
  return connection;
}

function createQuery(connection) {
  try {
    return {
      query(sql, args) {
        return promisify(connection.query).call(connection, sql, args);
      },
      close() {
        return promisify(connection.end).call(connection);
      },
      beginTransaction() {
        return promisify(connection.beginTransaction).call(connection);
      },
      commit() {
        return promisify(connection.commit).call(connection);
      },
      rollback() {
        return promisify(connection.rollback).call(connection);
      }
    };
  } catch (e) {
    console.log({ makeDb: e });
  }
}

module.exports = { params, createQuery, pool, promisifyQuery, getConnection };
